# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.2.5] - 2018-04-04
### Added
- Supports Table Rate Shipping, Weight Based Shipping and Woocommerce PDF Invoices
- Added Order Actions for manual usage (Regenerate label, order new label, order new barcode)
- Added Swiss Moon Delivery (possible slugs are "PostPac Express Mond", "PostPac Priority", "PostPac Economy")
- Option to manually generate labels / barcodes ondemand (processless)

### Changed
- Default Language German
- Better organization of settings panel

### Fixed
- Label is only outputted to the packing slip (when using PDF Invoices by Ewout Fernhout)
- "uploads/swisspost_labels" fixed chmod value to 766
- proper check if Weight Based Shipping Plugin is active
- On certain systems PRI mailing wouldn't work the proper way
