Local docker environment
-------
The docker-compose environment runs Mysql and Apache with Wordpress and this plugin's code.

Start up the environment:
```
docker-compose up -d
```

Run the one-time database setup:
```
docker-compose exec wordpress ./docker/setup-wordpress.sh
```

Navigate to http://localhost

### Wordpress admin
http://localhost/wp-admin (admin/admin)


Release a New Version
---------------------

This plugin is published to PHP Packagist at [thekitchenagency/swiss-post-labels](https://packagist.org/packages/thekitchenagency/swiss-post-labels).

To release a new version:
1. Tag the desired commit: `git tag v1.2.3`
2. Push the tag to GitLab: `git push git@gitlab.com:thekitchenagency/swiss-post-labels.git v1.2.3`
3. A GitLab-webhook will notify Packagist.
