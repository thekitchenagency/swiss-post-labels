<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

define('DB_NAME', 'wordpress');
define('DB_USER', 'root');
define('DB_PASSWORD', '');
define('DB_HOST', 'mysql');
define('DB_CHARSET', 'utf8');
define('DB_COLLATE', '');
$table_prefix  = 'wp_';

define('AUTH_KEY',         '471f54853953ae66eec730024a32fa837583c81d');
define('SECURE_AUTH_KEY',  'a9f337407d8a6c1b5de370eafc79dc88c74183b2');
define('LOGGED_IN_KEY',    'd626f40a777725c9e5a5d8fdcd970519e4046808');
define('NONCE_KEY',        'ea359d58b65ab59b18f8f59ac283baaa5b871c03');
define('AUTH_SALT',        '9aa704fe12c4b2cb509d95439f9fd86b03e57fe6');
define('SECURE_AUTH_SALT', '34c432ff791ecdc379f59aa4f6c7322c079e328b');
define('LOGGED_IN_SALT',   '40f318473542189ad2fed5e27a3deb8d20d7ef6f');
define('NONCE_SALT',       '38f955526988b35df3bf0a3a50039aebed5fde81');

define('WP_DEBUG', true);


/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
