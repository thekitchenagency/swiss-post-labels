#!/usr/bin/env bash

set -e
cd "$(dirname "$0")"

wait-for-it -t 30 mysql:3306

function wp() {
	/usr/local/bin/wp --allow-root "$@"
}

if ! (wp core is-installed); then
	wp core install --url=http://localhost --title=wordpress --admin_user=admin --admin_password=admin --admin_email=admin@example.com
	wp site empty --yes

	# Install woocommerce
	wp plugin install woocommerce --version=3.4.4 --activate
	wp plugin install test-gateway-for-woocommerce --version=1.5 --activate
	wp theme install storefront --version=2.3.3 --activate
	wp option update 'woocommerce_store_city' 'Zürich'
	wp option update 'woocommerce_store_postcode' '8001'
	wp option update 'woocommerce_default_country' 'CH'
	wp option update 'woocommerce_currency'  'CHF'
	wp option update 'woocommerce_weight_unit'  'g'
	wp option update 'woocommerce_subscriptions_enable_retry'  'yes'
	wp option update 'woocommerce_paypal_settings' '--format=json' '{"debug":"yes","enabled":"no","email":false}'
	wp option update 'woocommerce_admin_notices' '--format=json' '[]'
	wp option update 'woocommerce_product_type' 'physical'
	wp option update 'woocommerce_allow_tracking' 'no'
	wp option update 'woocommerce_cheque_settings' '--format=json' '{"enabled":"no"}'
	wp option update 'woocommerce_bacs_settings' '--format=json' '{"enabled":"no"}'
	wp option update 'woocommerce_cod_settings' '--format=json' '{"enabled":"no"}'
	wp option update 'woocommerce_flat_rate_1_settings' '--format=json' '{"title":"Flat rate","tax_status":"taxable","cost":"5"}'
	wp option update 'woocommerce_permalinks' '--format=json' '{"product_base":"\/shop","category_base":"product-category","tag_base":"product-tag","attribute_base":"","use_verbose_page_rules":true}'
	wp option update 'storefront_nux_dismissed' '1'

	# Install swiss-post-labels
	wp plugin activate swiss-post-labels
	wp option update 'CHL_wc__sender_name' 'My company'
	wp option update 'CHL_wc__sender_street' 'Bahnhofstrasse 1'
	wp option update 'CHL_wc__sender_zip' '8001'
	wp option update 'CHL_wc__sender_city' 'Zürich'
	wp option update 'CHL_wc__sender_domi' '8001 Zürich ZH'
	wp option update 'CHL_wc__username' 'barcode_username'
	wp option update 'CHL_wc__password' 'barcode_password'
	wp option update 'CHL_wc__franking' 'barcode_franking_license'
	wp option update 'CHL_wc__orderhook' 'processing'
	wp option update 'CHL_wc__print_addresses' 'OnlyRecipient'
	wp option update 'CHL_wc__label_size' 'A6'
	wp option update 'CHL_wc__format' 'barcode'
	wp option update 'CHL_wc__fileformat' 'pdf'
	wp option update 'CHL_wc__shiptype' 'ECO'
	wp option update 'CHL_wc__customerMessage' ''
	wp option update 'CHL_wc__wpipactive' 'no'

	# Create example product and order
	wp wc --user=1 tool run install_pages
	wp wc --user=1 product create --name="Testproduct" --slug="testproduct" --description="A test product" --type="simple" --regular_price="12.95"
	product_id=$(wp wc --user=1 product list --format=ids | head -n 1)
	wp wc --user=1 shop_order create --customer_id=0 --status=pending --line_items="[{\"product_id\": $product_id}]" --shipping='{"first_name": "Hans", "last_name": "Muster", "address_1": "Musterplatz 3", "city": "Basel", "postcode": "4000", "country": "CH"}'
fi
