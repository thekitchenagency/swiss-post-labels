<?php
/**
 * WooCommerce Swiss Post Settings
 *
 * @package     woocommerce-swiss-post-labels
 * @author      Tiago Dias (thekitchen.agency)
 * @category    Admin
 * @version     1.2.5
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'SwissPost_Labels_Settings' ) ) :

	class SwissPost_Labels_Settings extends WC_Settings_Page {

		public function __construct() {
			$this->id          = 'swiss_post';
			$this->label       = __( 'Swiss Post', CHL_WC_TXT );
			$this->wpip_active = false;

			/*if ( empty( get_option( 'chl_license' ) ) ) {
				wp_redirect( "admin.php?page=CHL_wc-welcome" );
			}*/

			add_filter( 'woocommerce_settings_tabs_array', array( $this, 'add_settings_page' ), 20 );
			add_action( 'woocommerce_settings_' . $this->id, array( $this, 'output' ) );
			add_action( 'woocommerce_settings_save_' . $this->id, array( $this, 'save' ) );

			add_action( 'woocommerce_sections_' . $this->id, array( $this, 'output_sections' ) );

			if ( is_plugin_active( 'woocommerce-pdf-invoices-packing-slips/woocommerce-pdf-invoices-packingslips.php' ) ) {
				$this->wpip_active = true;
			}
		}

		public function get_sections() {

			$sections = array(
				'chl_wc_general'  => __( 'Generell', CHL_WC_TXT ),
				'chl_wc_advanced' => __( 'Konfiguration', CHL_WC_TXT ),
				/*'chl_wc_info'     => __( 'Lizenz & Support', CHL_WC_TXT ),*/
			);

			return apply_filters( 'woocommerce_get_sections_' . $this->id, $sections );
		}


		public function add_settings_tab( $settings_tabs ) {
			if ( ! in_array( 'swiss_post', $settings_tabs ) ) {
				$settings_tabs['swiss_post'] = __( 'Swiss Post', 'woocommerce-settings-tab-demo' );
			}

			return $settings_tabs;
		}

		/**
		 * Get settings array
		 *
		 * @return array
		 */
		public function get_settings( $current_section = '' ) {
			if ( $current_section == 'chl_wc_general' ) {

				$settings = array(
					array(
						'title' => __( '', CHL_WC_TXT ),
						'type'  => 'title',
						'desc'  => '',

					),

					array(
						'title' => __( 'Absender Angaben', CHL_WC_TXT ),
						'type'  => 'title',
						'desc'  => __( '', CHL_WC_TXT ),
						'id'    => CHL_WC_DB . 'chlwc_title_1'
					),

					array(
						'title'    => __( 'Absender Name', CHL_WC_TXT ),
						'desc'     => __( '', CHL_WC_TXT ),
						'id'       => CHL_WC_DB . '_sender_name',
						'default'  => '',
						'type'     => 'text',
						'autoload' => false,
						'desc_tip' => true,
					),

					array(
						'title'    => __( 'Absender Strasse', CHL_WC_TXT ),
						'desc'     => __( '', CHL_WC_TXT ),
						'id'       => CHL_WC_DB . '_sender_street',
						'default'  => '',
						'type'     => 'text',
						'autoload' => false,
						'desc_tip' => true,
					),

					array(
						'title'    => __( 'Absender PLZ', CHL_WC_TXT ),
						'desc'     => __( '', CHL_WC_TXT ),
						'id'       => CHL_WC_DB . '_sender_zip',
						'default'  => '',
						'type'     => 'text',
						'autoload' => false,
						'desc_tip' => true,
					),

					array(
						'title'    => __( 'Absender Stadt', CHL_WC_TXT ),
						'desc'     => __( '', CHL_WC_TXT ),
						'id'       => CHL_WC_DB . '_sender_city',
						'default'  => '',
						'type'     => 'text',
						'autoload' => false,
						'desc_tip' => true,
					),

					array(
						'title'    => __( 'Absender PostDomizil', CHL_WC_TXT ),
						'desc'     => __( '', CHL_WC_TXT ),
						'id'       => CHL_WC_DB . '_sender_domi',
						'default'  => '',
						'type'     => 'text',
						'autoload' => false,
						'desc_tip' => true,
					),

					array( 'type' => 'sectionend', 'id' => 'general_absender' ),

					array(
						'title' => __( 'Webservice Barcode Daten', CHL_WC_TXT ),
						'type'  => 'title',
						'desc'  => __( 'Diese Informationen erhalten Sie direkt von der Post WebServices Hotline <a href="//www.post.ch/en/business/a-z-of-subjects/dropping-off-mail-items/business-sending-letters/barcode-support" target="_blank">Technischer Support Schweizer Post</a>', CHL_WC_TXT ),
					),

					array(
						'title'    => __( 'Username', CHL_WC_TXT ),
						'desc'     => __( '', CHL_WC_TXT ),
						'id'       => CHL_WC_DB . '_username',
						'default'  => '',
						'type'     => 'text',
						'autoload' => false,
						'desc_tip' => true,
					),

					array(
						'title'    => __( 'Password', CHL_WC_TXT ),
						'desc'     => __( '', CHL_WC_TXT ),
						'id'       => CHL_WC_DB . '_password',
						'default'  => '',
						'type'     => 'password',
						'desc_tip' => true,
					),

					array(
						'title'    => __( 'Franking License', CHL_WC_TXT ),
						'desc'     => __( '', CHL_WC_TXT ),
						'id'       => CHL_WC_DB . '_franking',
						'default'  => '',
						'type'     => 'text',
						'autoload' => false,
						'desc_tip' => true,
					),

					array( 'type' => 'sectionend', 'id' => 'general_swisspost_data' ),


					/*array(
						'name'    => __( 'Packing Slip', CHL_WC_TXT ),
						'desc'    => __( 'Auto-Generate Packing Slip as PDF', CHL_WC_TXT ),
						'id'      => CHL_WC_DB.'_packingslip_enabled',
						'default' => 'no',
						'type'    => 'checkbox'

					 ),*/


				);


				return $settings;

			} else if ( $current_section == 'chl_wc_advanced' ) {
				$settings = array(
					array(
						'title' => __( 'Konfiguration Pakete', CHL_WC_TXT ),
						'type'  => 'title',
						'desc'  => __( 'Hier können Sie verschiedene Anpassungen an das Plugin vornehmen.', CHL_WC_TXT ),
					),

					array(
						'title'    => __( 'Bestellprozess Status', CHL_WC_TXT ),
						'desc'     => __( 'Hook into WooCommerce Process or select "none" for custom generation', CHL_WC_TXT ),
						'id'       => CHL_WC_DB . '_orderhook',
						'default'  => 'all',
						'type'     => 'select',
						'class'    => 'wc-enhanced-select',
						'options'  => array(
							'manual'            => __( 'none', CHL_WC_TXT ),
							'processing'        => __( 'Processing', CHL_WC_TXT ),
							'completed'         => __( 'Completed', CHL_WC_TXT ),
							'payment-completed' => __( 'Payment Completed', CHL_WC_TXT )
						),
						'autoload' => false
					),

					array(
						'title'    => __( 'Print Addresses', CHL_WC_TXT ),
						'desc'     => __( 'Print Addresses', CHL_WC_TXT ),
						'id'       => CHL_WC_DB . '_print_addresses',
						'default'  => 'all',
						'type'     => 'select',
						'class'    => 'wc-enhanced-select',
						'options'  => array(
							'None'                 => __( 'None', CHL_WC_TXT ),
							'OnlyRecipient'        => __( 'Only Recipient', CHL_WC_TXT ),
							'OnlyCustomer'         => __( 'Only Customer', CHL_WC_TXT ),
							'RecipientAndCustomer' => __( 'Recipient and Customer', CHL_WC_TXT )
						),
						'autoload' => false
					),


					array(
						'title'    => __( 'Label Size', CHL_WC_TXT ),
						'desc'     => __( 'Label Size', CHL_WC_TXT ),
						'id'       => CHL_WC_DB . '_label_size',
						'default'  => 'all',
						'type'     => 'select',
						'class'    => 'wc-enhanced-select',
						'options'  => array(
							'A5' => __( 'A5', CHL_WC_TXT ),
							'A6' => __( 'A6', CHL_WC_TXT ),
							'A7' => __( 'A7', CHL_WC_TXT )
						),
						'autoload' => false
					),
					array(
						'title'    => __( 'Format', CHL_WC_TXT ),
						'desc'     => __( 'Label Format', CHL_WC_TXT ),
						'id'       => CHL_WC_DB . '_format',
						'default'  => 'all',
						'type'     => 'select',
						'class'    => 'wc-enhanced-select',
						'options'  => array(
							'barcode'        => __( 'Barcode', CHL_WC_TXT ),
							'single_barcode' => __( 'Single Barcode', CHL_WC_TXT ),
							'label'          => __( 'Label', CHL_WC_TXT )

						),
						'autoload' => false
					),
					array(
						'title'    => __( 'File Format', CHL_WC_TXT ),
						'desc'     => __( 'File Format to be saved as, <b>do not use PDF in conjunction with WPIP</b>', CHL_WC_TXT ),
						'id'       => CHL_WC_DB . '_fileformat',
						'default'  => 'all',
						'type'     => 'select',
						'class'    => 'wc-enhanced-select',
						'options'  => array(
							'eps'  => __( 'eps', CHL_WC_TXT ),
							'gif'  => __( 'gif', CHL_WC_TXT ),
							'png'  => __( 'png', CHL_WC_TXT ),
							'pdf'  => __( 'pdf', CHL_WC_TXT ),
							'zpl2' => __( 'zpl2', CHL_WC_TXT )
						),
						'autoload' => false
					),


					array(
						'title'    => __( 'Default Shipment Type', CHL_WC_TXT ),
						'desc'     => __( 'Installing Weight Based Shipping disables this option. Recommended install: <a href="//wordpress.org/plugins/woocommerce-pdf-invoices-packing-slips/" target="_blank">Download here</a> ', CHL_WC_TXT ),
						'id'       => CHL_WC_DB . '_shiptype',
						'default'  => 'all',
						'type'     => 'select',
						'class'    => 'wc-enhanced-select',
						'options'  => array(
							'ECO' => __( 'Postpac Economy', CHL_WC_TXT ),
							'PRI' => __( 'Postpac Priority', CHL_WC_TXT ),
							'SEM' => __( 'Swiss Express "Mond"', CHL_WC_TXT )
						),
						'autoload' => false
					),

					array(
						'title'    => __( 'Customer Message Tracking', CHL_WC_TXT ),
						'desc'     => __( 'Use the [TRACKING_LINK=the message that will be wrapped by the href tag] merge tag to implement the appropriate href link to swiss post', CHL_WC_TXT ),
						'id'       => CHL_WC_DB . '_customerMessage',
						'default'  => '',
						'css'      => 'width:75%;',
						'type'     => 'textarea',
						'autoload' => false,
						'desc_tip' => false,
					),


					array(
						'name'    => __( 'WPIP Integration', CHL_WC_TXT ),
						'desc'    => __( '', CHL_WC_TXT ),
						'id'      => CHL_WC_DB . '_wpipactive',
						'default' => 'no',
						'type'    => 'checkbox'
					),


					array(
						'type' => 'sectionend',
						'id'   => 'swisspost_labels_settings_x_end'
					),

					/*array(
						'title' => __( 'Konfiguration Briefe', CHL_WC_TXT ),
						'type' => 'title',
						'desc' => __( 'Hier können Sie verschiedene Anpassungen an das Plugin vornehmen.', CHL_WC_TXT ),
					),

					array(
						'title'    => __( 'Label Size', CHL_WC_TXT ),
						'desc'     =>  __( 'Label Size', CHL_WC_TXT ),
						'id'       => CHL_WC_DB.'_label_size__letters',
						'default'  => 'all',
						'type'     => 'select',
						'disabled'	=> true,
						'class'    => 'wc-enhanced-select',
						'options' => array(
							'FE' => __('FE',CHL_WC_TXT)
						),
						'custom_attributes' => array(
							'disabled' => true,
						),
						'autoload' => false
					),

					array(
						'title'    => __( 'Print Addresses', CHL_WC_TXT ),
						'desc'     =>  __( 'Print Addresses', CHL_WC_TXT ),
						'id'       => CHL_WC_DB.'_print_addresses__letters',
						'default'  => 'all',
						'type'     => 'select',
						'class'    => 'wc-enhanced-select',
						'options' => array(
							'None' => __('None',CHL_WC_TXT),
							'OnlyRecipient' => __('Only Recipient',CHL_WC_TXT),
							'OnlyCustomer' => __('Only Customer',CHL_WC_TXT),
							'RecipientAndCustomer' => __('Recipient and Customer',CHL_WC_TXT)
						),

						'autoload' => false
					),

					array(
						'title'    => __( 'File Format', CHL_WC_TXT ),
						'desc'     =>  __( 'File Format to be saved as, <b>do not use PDF in conjunction with WPIP</b>', CHL_WC_TXT ),
						'id'       => CHL_WC_DB.'_fileformat__letters',
						'default'  => 'all',
						'type'     => 'select',
						'class'    => 'wc-enhanced-select',
						'options' => array(
							'eps' => __('eps',CHL_WC_TXT),
							'gif' => __('gif',CHL_WC_TXT),
							'png' => __('png',CHL_WC_TXT),
							'pdf' => __('pdf',CHL_WC_TXT),
							'zpl2' => __('zpl2', CHL_WC_TXT)
						),
						'autoload' => false
					),

					array(
						'title'    => __( 'Default Shipment Type', CHL_WC_TXT ),
						'desc'     =>  __( 'Installing Weight Based Shipping disables this option. Recommended install: <a href="//wordpress.org/plugins/woocommerce-pdf-invoices-packing-slips/" target="_blank">Download here</a> ', CHL_WC_TXT ),
						'id'       => CHL_WC_DB.'_shiptype__letters',
						'default'  => 'all',
						'type'     => 'select',
						'class'    => 'wc-enhanced-select',
						'options' => array(
							'APOST' => __('A POST',CHL_WC_TXT),
							'BPOST' => __('B POST',CHL_WC_TXT),
							'RINL' => __('Eingeschrieben',CHL_WC_TXT)
						),
						'autoload' => false
					),

					array(
						'type' => 'sectionend',
						'id' => 'swisspost_labels_settings_x_end'
					),
	*/


				);

				return $settings;
			} else if ( $current_section == 'chl_wc_info' ) {
				$settings = array(


					array(
						'title' => __( 'Support', CHL_WC_TXT ),
						'type'  => 'title',
						'desc'  => __( 'Benötigen Sie Hilfe oder Unterstützung dann schreiben Sie uns eine<a href="mailto:incoming+thekitchenagency/woocommerce-swisspost-labels@gitlab.com"> E-Mail</a>', CHL_WC_TXT ),
					),
					
					array(
						'type' => 'sectionend',
						'id'   => 'swisspost_labels_settings_x_end'
					),
				);

				return $settings;
			} else {
				return $settings;
			}
		}

		public function output() {

			global $current_section;

			//$current_section = ($current_section == NULL) ? 'chl_wc_general' : $current_section;

			$settings = $this->get_settings( $current_section );
			WC_Admin_Settings::output_fields( $settings );
		}

		/**
		 * Save settings
		 */
		public function save() {
			global $current_section;

			$settings = $this->get_settings( $current_section );
			WC_Admin_Settings::save_fields( $settings );
		}
	}

endif;

return new SwissPost_Labels_Settings();
?>