<?php

if ( ! defined( 'WPINC' ) ) {
	die;
}

require "chpost_utils.php";

class SwissPost_Labels_Admin_Function {
	public function __construct() {
		global $swisspost_utils;


		add_action( 'woocommerce_order_status_processing', array( $this, 'chpost_order_processing' ) );
		add_action( 'woocommerce_order_status_completed', array( $this, 'chpost_order_completed' ) );
		add_action( 'woocommerce_payment_complete', array( $this, 'chpost_order_payment_complete' ) );

		add_filter( 'woocommerce_payment_complete_order_status', array( $this, 'chpost_order_payment_complete_cc' ) );

		add_filter( 'manage_edit-shop_order_columns', array( $this, 'add_chpost_labels_column' ) );
		add_action( 'manage_shop_order_posts_custom_column', array( $this, 'populate_chpost_values' ), 2 );
		add_action( 'woocommerce_email_before_order_table', array( $this, 'add_tracktraceID_instructions' ), 3, 4 );

		if ( is_plugin_active( 'woocommerce-pdf-invoices-packing-slips/woocommerce-pdf-invoices-packingslips.php' ) ) {
			add_action( 'wpo_wcpdf_before_document', array( $this, "updatePDF" ), 2, 2 );
		}

		/* SINCE 1.2.5 */
		add_action( 'woocommerce_order_actions', array( $this, 'tka_wc_add_order_meta_box_action' ) );
		add_action( 'woocommerce_order_action_chl_regenerate_label', array( $this, 'tka_wc_regenerate_label_action' ) );
		add_action( 'woocommerce_order_action_chl_order_label', array( $this, 'tka_wc_order_label_action' ) );
		add_action( 'woocommerce_order_action_chl_order_barcode', array( $this, 'tka_wc_order_barcode_action' ) );
	}

	function add_tracktraceID_instructions( $order, $sent_to_admin, $plain_text, $email ) {
		global $post;
		$data      = get_post_meta( $post->ID );
		$ship_meth = $order->get_shipping_method();

		//if(strpos( $ship_meth, 'PostPac' ) !== false){
		if ( get_option( 'CHL_wc__customerMessage' ) != "" ) {
			$customMessage = get_option( 'CHL_wc__customerMessage' );
			$hrefLink      = $this->get_string_between( $customMessage, "=", "]" );

			$mergeTagSP = '<a href="https://service.post.ch/EasyTrack/submitParcelData.do?formattedParcelCodes=' . $data["chpost_tracking_id_1"][0] . '&lang=&shortcut=swisspost-tracking" target="_blank">';
			$mergeTagSP .= $hrefLink;
			$mergeTagSP .= '</a>';

			$newMsg = $this->advanced_replace( '[', ']', $mergeTagSP, $customMessage );

			echo '<p>';
			echo $newMsg;
			echo '</p>';
		} else {
			echo '<p><strong>Verfolgen Sie Ihre Sendung</strong><br/><a href="https://service.post.ch/EasyTrack/submitParcelData.do?formattedParcelCodes=' . $data['chpost_tracking_id_1'][0] . '&lang=&shortcut=swisspost-tracking" target="_blank">Klicken Sie hier um Ihre Bestellung zu verfolgen</a></p>';
		}

		// }
	}

	function get_string_between( $string, $start, $end ) {
		$string = ' ' . $string;
		$ini    = strpos( $string, $start );
		if ( $ini == 0 ) {
			return '';
		}
		$ini += strlen( $start );
		$len = strpos( $string, $end, $ini ) - $ini;

		return substr( $string, $ini, $len );
	}

	function advanced_replace( $searchStart, $searchEnd, $replace, $subject, &$assignValue = array(), $addValue = false, $inReplace = false, $valueKey = "" ) {
		$strlen     = strlen( $subject );
		$open       = 0;
		$ob         = false;
		$ob_message = "";
		$message    = "";
		for ( $i = 0; $i <= $strlen; $i ++ ) {
			$char = substr( $subject, $i, 1 );

			if ( $char == $searchStart ) {
				$open ++;
				$ob = true;
			}
			if ( $ob ) {
				$ob_message .= $char;
			} else {
				$message .= $char;
			}
			if ( $char == $searchEnd ) {
				$open --;
				if ( $open == 0 ) {
					$ob                                                                  = false;
					$message                                                             .= ( $replace . ( $addValue !== false && $inReplace ? $addValue : "" ) );
					$assignValue[ $valueKey . ( $addValue !== false ? $addValue : "" ) ] = $ob_message;
					$ob_message                                                          = "";
					if ( $addValue !== false ) {
						$addValue ++;
					}
				}
			}
		}

		return $message;
	}

	function updatePDF( $template, $order_data ) {
		if ( class_exists( "PC" ) ) {
			PC::debug( $template );
		}
		if ( $template == "packing-slip" ) {
			$upload_dir = wp_upload_dir();
			if ( get_post_meta( $order_data->id, 'chpost_tracking_id', true ) == null ) {
				echo '<img style="width:126%;display:block;height:auto;" src="' . $upload_dir['baseurl'] . '/swisspost_labels/' . get_post_meta( $order_data->id, 'chpost_tracking_id_1', true ) . "." . get_option( 'CHL_wc__fileformat' ) . '" />';
				echo '<div style="page-break-before: always;"></div>';
			} else {
				echo '<img style="width:126%;display:block;height:auto;" src="' . $upload_dir['baseurl'] . '/swisspost_labels/' . get_post_meta( $order_data->id, 'chpost_tracking_id', true ) . "." . get_option( 'CHL_wc__fileformat' ) . '" />';
				echo '<div style="page-break-before: always;"></div>';
			}
		}


	}


	function chpost_order_processing( $order_id ) {
		if ( get_option( 'CHL_wc__orderhook' ) == "processing" ) {
			global $swisspost_utils;
			$swisspost_utils = new WC_SwissPostUtils();
			$obj             = $swisspost_utils->getUserShippingDetailsFromOrderID( $order_id );
		}
	}

	function chpost_order_completed( $order_id ) {
		if ( get_option( 'CHL_wc__orderhook' ) == "completed" ) {
			global $swisspost_utils;
			$swisspost_utils = new WC_SwissPostUtils();
			$obj             = $swisspost_utils->getUserShippingDetailsFromOrderID( $order_id );
		}
	}

	function chpost_order_payment_complete( $order_id ) {
		if ( get_option( 'CHL_wc__orderhook' ) == "payment-completed" ) {
			global $swisspost_utils;
			$swisspost_utils = new WC_SwissPostUtils();
			$obj             = $swisspost_utils->getUserShippingDetailsFromOrderID( $order_id );
		}
	}

	function chpost_order_payment_complete_cc( $order_id ) {
		global $swisspost_utils;
		$swisspost_utils = new WC_SwissPostUtils();
		$obj             = $swisspost_utils->getUserShippingDetailsFromOrderID( $order_id );
	}


	function add_chpost_labels_column( $columns ) {
		$new_columns = ( is_array( $columns ) ) ? $columns : array();
		unset( $new_columns['order_actions'] );
		$new_columns['chpost']        = 'SwissPost';
		$new_columns['order_actions'] = $columns['order_actions'];

		return $new_columns;
	}

	function populate_chpost_values( $column ) {
		global $post;
		$data       = get_post_meta( $post->ID );
		$upload_dir = wp_upload_dir();
		if ( $column == 'chpost' ) {
			echo( isset( $data['chpost_tracking_id_1'][0] ) ? '<a href="' . $upload_dir["baseurl"] . "/swisspost_labels/" . $data["chpost_tracking_id_1"][0] . '.' . get_option( 'CHL_wc__fileformat' ) . '" target="_blank">Print Label</a><br/>' : '' );
		}
	}

	/**
	 * Add a custom action to order actions select box on edit order page
	 * Only added for paid orders that haven't fired this action yet
	 *
	 * @param array $actions order actions array to display
	 *
	 * @return array - updated actions
	 */
	function tka_wc_add_order_meta_box_action( $actions ) {
		global $theorder;

		// bail if the order has been paid for or this action has been run
		/*if ( ! $theorder->is_paid() || get_post_meta( $theorder->id, '_wc_order_marked_printed_for_packaging', true ) ) {
			return $actions;
		}*/

		// add "mark printed" custom action
		$actions['chl_regenerate_label'] = __( 'CHL - RE-generate Label', CHL_WC_TXT );
		$actions['chl_order_label']      = __( 'CHL - Order Label', CHL_WC_TXT );
		$actions['chl_order_barcode']    = __( 'CHL - Order Barcode', CHL_WC_TXT );

		return $actions;
	}

	/**
	 * Add an order note when custom action is clicked
	 * Add a flag on the order to show it's been run
	 *
	 * @param \WC_Order $order
	 */
	function tka_wc_regenerate_label_action( $order ) {
		global $swisspost_utils;

		//remove meta
		delete_post_meta( $order->ID, 'chpost_data' );
		delete_post_meta( $order->ID, 'chpost_tracking_id_1' );

		// add the order note
		// translators: Placeholders: %s is a user's display name
		$message = sprintf( __( 'CHL - Label / Barcode RE-generated', CHL_WC_TXT ), wp_get_current_user()->display_name );
		$order->add_order_note( $message );

		$swisspost_utils = new WC_SwissPostUtils();
		$obj             = $swisspost_utils->getUserShippingDetailsFromOrderID( $order->ID );


		// add the flag
		//update_post_meta( $order->id, '_wc_order_marked_printed_for_packaging', 'yes' );
	}

	function tka_wc_order_label_action( $order ) {

		// add the order note
		// translators: Placeholders: %s is a user's display name
		$message = sprintf( __( 'CHL - Order Label', CHL_WC_TXT ), wp_get_current_user()->display_name );
		$order->add_order_note( $message );

		// add the flag
		//update_post_meta( $order->id, '_wc_order_marked_printed_for_packaging', 'yes' );
	}

	function tka_wc_order_barcode_action( $order ) {

		// add the order note
		// translators: Placeholders: %s is a user's display name
		$message = sprintf( __( 'CHL - Order Barcode', CHL_WC_TXT ), wp_get_current_user()->display_name );
		$order->add_order_note( $message );

		$swisspost_utils = new WC_SwissPostUtils();
		$obj             = $swisspost_utils->getUserShippingDetailsFromOrderID( $order->ID, "letter-barcode" );

		// add the flag
		//update_post_meta( $order->id, '_wc_order_marked_printed_for_packaging', 'yes' );
	}
}

?>