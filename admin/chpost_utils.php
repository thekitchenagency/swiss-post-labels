<?php

class WC_SwissPostUtils {
	private $username = '';
	private $password = '';
	private $franking = '';
	private $host = '';
	private $client;
	public $labelData;
	private $preg = "";

	public function __construct() {


		$this->username = get_option( 'CHL_wc__username' );
		$this->password = get_option( 'CHL_wc__password' );
		$this->franking = get_option( 'CHL_wc__franking' );
		$this->host     = ( get_option( 'CHL_wc__debug' ) == "yes" ) ? "https://wsbc.post.ch/wsbc/barcode/v2_3" : "https://wsbc.post.ch/wsbc/barcode/v2_3";

		$this->createSoapClient( $this->username, $this->password, $this->host );
	}

	public function __destruct() {

	}

	public function getElements( $root ) {
		if ( $root == null ) {
			return array();
		}
		if ( is_array( $root ) ) {
			return $root;
		} else {
			return array( $root );
		}
	}

	public function toCommaSeparatedString( $strings ) {
		$res       = "";
		$delimiter = "";
		foreach ( $strings as $str ) {
			$res       .= $delimiter . $str;
			$delimiter = ", ";
		}

		return $res;
	}

	public function createSoapClient( $username, $password, $host ) {
		$SOAP_wsdl_file_path = __DIR__ . '/barcode_v2_3.wsdl';

		$SOAP_config = array(
			'location'           => $host,
			'login'              => $username,
			'password'           => $password,
			'trace'              => 0,
			'connection_timeout' => 90
		);

		try {
			$this->client = new SoapClient( $SOAP_wsdl_file_path, $SOAP_config );

			if ( get_option( 'wc_chlabels_debug1' ) == "yes" ) {
				echo "SOAP-CLIENT:";
				echo '<pre>';
				var_dump( $this->client );
				echo '</pre>';
			}
		} catch ( SoapFault $fault ) {
			echo( 'Error in SOAP Initialization: ' . $fault->__toString() . '<br/>' );
			exit;
		}
	}

	public function getUserShippingDetailsFromOrderID( $id, $labelType = null ) {
		$re  = "/(\\w+.*)(?:\\s|$)(\\d+\\w?)/";
		$str = get_post_meta( $id, '_shipping_address_1', true );

		$this->preg = preg_match_all( $re, $str, $matches );

		$order = new WC_Order( $id );

		$labelData             = new WC_SwissPostLabelData();
		$labelData->orderId    = $id;
		$labelData->first_name = get_post_meta( $id, '_shipping_first_name', true );
		$labelData->last_name  = get_post_meta( $id, '_shipping_last_name', true );
		$labelData->company    = get_post_meta( $id, '_shipping_company', true );

		if ( implode( $matches[0] ) === '' ) {
			$labelData->po_box = get_post_meta( $id, '_shipping_address_1', true );
		} else {
			$labelData->street    = implode( $matches[1] );
			$labelData->street_nr = implode( $matches[2] );
		}
		$labelData->zip                = get_post_meta( $id, '_shipping_postcode', true );
		$labelData->city               = get_post_meta( $id, '_shipping_city', true );
		$labelData->country            = get_post_meta( $id, '_shipping_country', true );
		$labelData->chpost_tracking_id = "";

		include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

		if ( is_plugin_active( 'weight-based-shipping-for-woocommerce/plugin.php' ) ) {
			$labelData->shipping_method = $this->get_chpost_shipping_method( $order->get_shipping_method() );
		} else if ( class_exists( 'BE_Table_Rate_WC' ) ) {
			$labelData->shipping_method = $this->get_chpost_shipping_method( $order->get_shipping_method() );
		} else {
			$labelData->shipping_method = get_option( 'CHL_wc__shiptype' );
		}

		$labelData->payment_method = $order->get_payment_method();
		$labelData->cart_total     = $order->get_total();
		$this->labelData           = $labelData;

		//Create Label
		if ( $labelType != null && $labelType == "letter-barcode" ) {
			$this->generateShippingLabel( true );
		} else {
			$this->generateShippingLabel();
		}


		add_post_meta( $id, 'chpost_data', serialize( $labelData ), true );

		return $labelData;
	}

	public function get_chpost_shipping_method( $sm ) {
		if ( $sm == "PostPac Priority" ) {
			return "PRI";
		} else if ( $sm == "PostPac Express Mond" ) {
			return "SEM";
		} else {
			return "ECO";
		}
	}

	public function generateShippingLabel( $isLetter = false ) {
		$obj = $this->labelData;
		if ( class_exists( "PC" ) ) {
			PC::debug( $obj );
		}
		$this->generateLabelRequest( $obj );

	}

	private function generateLabelRequest( $data ) {

		//COD - Cash On Delivery
		$cod = $data->payment_method;

		$generateLabelRequest['Language']                                       = 'de';
		$generateLabelRequest['Envelope']['LabelDefinition']['LabelLayout']     = strtoupper( get_option( 'CHL_wc__label_size' ) );
		$generateLabelRequest['Envelope']['LabelDefinition']['PrintAddresses']  = get_option( 'CHL_wc__print_addresses' );
		$generateLabelRequest['Envelope']['LabelDefinition']['ImageFileType']   = get_option( 'CHL_wc__fileformat' );
		$generateLabelRequest['Envelope']['LabelDefinition']['ImageResolution'] = 300;
		$generateLabelRequest['Envelope']['LabelDefinition']['PrintPreview']    = false;

		$generateLabelRequest['Envelope']['FileInfos']['FrankingLicense']                                               = $this->franking;
		$generateLabelRequest['Envelope']['FileInfos']['PpFranking']                                                    = false;
		$generateLabelRequest['Envelope']['FileInfos']['Customer']['Name1']                                             = get_option( 'CHL_wc__sender_name' );
		$generateLabelRequest['Envelope']['FileInfos']['Customer']['Street']                                            = get_option( 'CHL_wc__sender_street' );
		$generateLabelRequest['Envelope']['FileInfos']['Customer']['ZIP']                                               = get_option( 'CHL_wc__sender_zip' );
		$generateLabelRequest['Envelope']['FileInfos']['Customer']['City']                                              = get_option( 'CHL_wc__sender_city' );
		$generateLabelRequest['Envelope']['FileInfos']['Customer']['Country']                                           = $data->country;
		$generateLabelRequest['Envelope']['FileInfos']['Customer']['DomicilePostOffice']                                = get_option( 'CHL_wc__sender_domi' );
		$generateLabelRequest['Envelope']['FileInfos']['CustomerSystem']                                                = 'Woocommerce Integration tKA';
		$generateLabelRequest['Envelope']['Data']['Provider']['Sending']['Item'][0]['ItemID']                           = $data->orderId;
		$generateLabelRequest['Envelope']['Data']['Provider']['Sending']['Item'][0]['Recipient']['PersonallyAddressed'] = false;
		$generateLabelRequest['Envelope']['Data']['Provider']['Sending']['Item'][0]['Recipient']['Name1']               = $data->first_name . ' ' . $data->last_name;
		$generateLabelRequest['Envelope']['Data']['Provider']['Sending']['Item'][0]['Recipient']['Name2']               = $data->company;

		if ( $this->preg ) {
			$generateLabelRequest['Envelope']['Data']['Provider']['Sending']['Item'][0]['Recipient']['Street']  = $data->street;
			$generateLabelRequest['Envelope']['Data']['Provider']['Sending']['Item'][0]['Recipient']['HouseNo'] = $data->street_nr;
		} else {
			$generateLabelRequest['Envelope']['Data']['Provider']['Sending']['Item'][0]['Recipient']['POBox'] = $data->po_box;
		}

		$generateLabelRequest['Envelope']['Data']['Provider']['Sending']['Item'][0]['Recipient']['ZIP']       = $data->zip;
		$generateLabelRequest['Envelope']['Data']['Provider']['Sending']['Item'][0]['Recipient']['City']      = $data->city;
		$generateLabelRequest['Envelope']['Data']['Provider']['Sending']['Item'][0]['Recipient']['Country']   = 'CH';
		$generateLabelRequest['Envelope']['Data']['Provider']['Sending']['Item'][0]['Attributes']['PRZL']     = array( $data->shipping_method );
		$generateLabelRequest['Envelope']['Data']['Provider']['Sending']['Item'][0]['Attributes']['FreeText'] = ( get_option( 'CHL_wc__msg' ) != '' ) ? get_option( 'CHL_wc__msg' ) : '';
		$generateLabelRequest['Envelope']['Data']['Provider']['Sending']['Item'][0]['Attributes']['ProClima'] = ( get_option( 'CHL_wc__proclima' ) == "yes" ) ? true : false;

		try {
			$response = $this->client->GenerateLabel( $generateLabelRequest );
		} catch ( SoapFault $fault ) {
			error_log( 'Error in GenerateLabel: ' . $fault->__toString() . '<br />' );
			if ( class_exists( "PC" ) ) {
				PC::debug( $fault->__toString() );
			}
		}

		foreach ( $this->getElements( $response->Envelope->Data->Provider->Sending->Item ) as $item ) {
			if ( class_exists( "PC" ) ) {
				PC::debug( $item );
			}
			if ( $item->Errors != null ) {
				$errorMessages = "";
				$delimiter     = "";

				foreach ( $this->getElements( $item->Errors->Error ) as $error ) {
					$errorMessages .= $delimiter . $error->Message;
					$delimiter     = ",";
				}

				error_log( '<p>ERROR for item with itemID=' . $item->ItemID . ": " . $errorMessages . '.<br/></p>' );
			} else {

				$identCode       = $item->IdentCode;
				$labelBinaryData = $item->Label;

				$upload_dir = wp_upload_dir();
				$folder_loc = $upload_dir['basedir'] . '/swisspost_labels/';
				$filename   = $identCode . '.' . get_option( 'CHL_wc__fileformat' );
				$filn       = $folder_loc . $filename;

				if ( ! is_dir( $folder_loc ) ) {
					if ( false === mkdir( $folder_loc, 0766 ) ) {
						throw new Exception( "could not create folder " . $folder_loc );
					}
				}
				if ( false === file_put_contents( $filn, $labelBinaryData ) ) {
					throw new Exception( "could not write to " . $filn );
				}
			}
		}

		//$this->createOrder($filn, $identCode);
		$this->update_chlabels_order_tracking( $data->orderId, $identCode );
	}

	private function update_chlabels_order_tracking( $id, $chpost_nr ) {
		add_post_meta( $id, 'chpost_tracking_id_1', $chpost_nr, true );
	}
}

class WC_SwissPostLabelData {
	public $first_name;
	public $last_name;
	public $company;
	public $street;
	public $street_nr;
	public $zip;
	public $city;
	public $country;
	public $shipping_type;
	public $orderId;
	public $po_box;
	public $chpost_tracking_id;
	public $chpost_order_id;
	public $cart_total;
	public $payment_method;

}

?>
