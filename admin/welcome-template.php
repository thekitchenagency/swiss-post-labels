<?php 
    if ( isset( $_REQUEST['activate_license'] ) ) {
        exit( wp_redirect( admin_url( 'admin.php?page=wc-settings&tab=swiss_post&section=chl_wc_general' ) ) );
    }


?>
<div class="wrap about-wrap">
    <h1>Welcome To <?php _e( CHL_WC_NAME . ' v ' . CHL_WC_V, CHL_WC_TXT ); ?></h1>
    <div class="about-text plugin_welcome_text">
        Thanks for installing! <?php _e( CHL_WC_NAME . ' v ' . CHL_WC_V, CHL_WC_TXT ); ?><br/>
        In case you find any Bugs or you have any problems, please write us an email <a
                href="mailto:incoming+thekitchenagency/woocommerce-swisspost-labels@gitlab.com">here</a></div>

	<?= '<div class="wrap"><hr/>'; ?>
    <form action="" method="post">
        <p class="submit">
            <input type="submit" name="activate_license" value="Continue" class="button-primary"/>
        </p>
    </form>
	<?= '</div>'; ?>
</div>