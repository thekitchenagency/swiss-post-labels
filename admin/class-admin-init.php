<?php
/**
 * The admin-specific functionality of the plugin.
 *
 * @package    @TODO
 * @subpackage @TODO
 * @author     thekitchen.agency <tech@thekitchen.agency>
 */
if ( ! defined( 'WPINC' ) ) {
	die;
}


class SwissPost_Labels_Admin extends SwissPostLabels {

	/**
	 * Initialize the class and set its properties.
	 * @since      0.1
	 */
	public function __construct() {
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_styles' ), 99 );
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		add_filter( 'plugin_row_meta', array( $this, 'plugin_row_links' ), 10, 2 );
		add_action( 'admin_init', array( $this, 'admin_init' ) );
		add_action( 'plugins_loaded', array( $this, 'init' ) );
		add_filter( 'woocommerce_get_settings_pages', array( $this, 'settings_page' ) );
	}

	/**
	 * Inits Admin Sttings
	 */
	public function admin_init() {
		new SwissPost_Labels_Admin_Function;
	}


	/**
	 * Add a new integration to WooCommerce.
	 */
	public function settings_page( $integrations ) {
		foreach ( glob( CHL_WC_PATH . 'admin/swisspost-settings*.php' ) as $file ) {
			$integrations[] = require_once( $file );
		}

		return $integrations;
	}

	/**
	 * Register the stylesheets for the admin area.
	 */
	public function enqueue_styles() {
		if ( in_array( $this->current_screen(), $this->get_screen_ids() ) ) {
			wp_enqueue_style( CHL_WC_SLUG . '_core_style', plugins_url( 'css/style.css', __FILE__ ), array(), $this->version, 'all' );
		}
	}


	/**
	 * Register the JavaScript for the admin area.
	 */
	public function enqueue_scripts() {
		if ( in_array( $this->current_screen(), $this->get_screen_ids() ) ) {
			wp_enqueue_media();
			wp_enqueue_script( CHL_WC_SLUG . '_core_script', plugins_url( 'js/script.js', __FILE__ ), array( 'jquery' ), $this->version, false );
		}

	}

	/**
	 * Gets Current Screen ID from wordpress
	 * @return string [Current Screen ID]
	 */
	public function current_screen() {
		$screen = get_current_screen();

		return $screen->id;
	}

	/**
	 * Returns Predefined Screen IDS
	 * @return [Array]
	 */
	public function get_screen_ids() {
		$screen_ids   = array();
		$screen_ids[] = 'edit-swisspost-labels';

		return $screen_ids;
	}


	/**
	 * Adds Some Plugin Options
	 *
	 * @param  array $plugin_meta
	 * @param  string $plugin_file
	 *
	 * @since 0.11
	 * @return array
	 */
	public function plugin_row_links( $plugin_meta, $plugin_file ) {
		if ( CHL_WC_FILE == $plugin_file ) {
			$plugin_meta[] = sprintf( '<a href="%s">%s</a>', admin_url( 'admin.php?page=wc-settings&tab=swiss_post' ), __( 'Settings', CHL_WC_TXT ) );
			$plugin_meta[] = sprintf( '<a href="%s">%s</a>', admin_url( 'admin.php?page=CHL_wc-welcome' ), __( 'License Key', CHL_WC_TXT ) );
			$plugin_meta[] = sprintf( '<a href="%s">%s</a>', 'mailto:tech@thekitchen.agency', __( 'Contact Author', CHL_WC_TXT ) );
		}

		return $plugin_meta;
	}
}

?>