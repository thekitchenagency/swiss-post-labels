<?php
/*
Plugin Name: Woocommerce - Swiss Post Labels
Plugin URI: https://thekitchen.agency/tka_products/woocommerce-swisspost-labels/
Description: A simple to use but versatile plugin to auto generate conform Swiss Post Barcode Labels. 
Author: thekitchen.agency
Version: 1.2.5
Author URI: https://thekitchen.agency
*/
if ( ! defined( 'WPINC' ) ) {
	die;
}

class SwissPostLabels {
	/**
	 * @var string
	 */
	public $version = '1.2.5';

	/**
	 * @var WooCommerce The single instance of the class
	 * @since 2.1
	 */
	protected static $_instance = null;

	protected static $functions = null;

	/**
	 * Creates or returns an instance of this class.
	 */
	public static function get_instance() {
		if ( null == self::$_instance ) {
			self::$_instance = new self;
		}

		return self::$_instance;
	}

	/**
	 * Class Constructor
	 */
	public function __construct() {
		$this->define_constant();
		$this->load_required_files();
		$this->init_class();
		add_action( 'init', array( $this, 'init' ) );
		__( "test", 'woocommerce-swisspost-labels' );
	}

	/**
	 * Triggers When INIT Action Called
	 */
	public function init() {
		add_action( 'plugins_loaded', array( $this, 'after_plugins_loaded' ) );
		add_filter( 'load_textdomain_mofile', array( $this, 'load_plugin_mo_files' ), 10, 2 );
		add_action( 'admin_enqueue_scripts', array( $this, 'load_custom_wp_admin_style' ) );
	}

	public function load_custom_wp_admin_style() {
		wp_register_style( 'custom_wp_admin_css', plugin_dir_url( __FILE__ ) . '/admin/css/style.css', false, '1.0.0' );
		wp_enqueue_style( 'custom_wp_admin_css' );

		wp_register_style( 'toastr_css', plugin_dir_url( __FILE__ ) . '/admin/css/toastr.min.css', false, '1.0.0' );
		wp_enqueue_style( 'toastr_css' );

		wp_enqueue_script( 'toastr-js', plugin_dir_url( __FILE__ ) . '/admin/js/toastr.min.js', false, '1.0.0', false );
		wp_enqueue_style( 'toastr-js' );
	}

	/**
	 * Loads Required Plugins For Plugin
	 */
	private function load_required_files() {
		if ( $this->is_request( 'admin' ) ) {
			$this->load_files( CHL_WC_PATH . 'admin/class-*.php' );
		}

	}

	/**
	 * Inits loaded Class
	 */
	private function init_class() {


		if ( $this->is_request( 'admin' ) ) {
			$this->admin = new SwissPost_Labels_Admin;

			new SwissPost_Labels_Activation( 'CHL_wc_', 'CHL_wc-welcome', 'welcome-template.php', 'Welcome To SwissPostLabels', __FILE__ );
		}
	}

	protected function load_files( $path, $type = 'require' ) {
		foreach ( glob( $path ) as $files ) {

			if ( $type == 'require' ) {
				require_once( $files );
			} else if ( $type == 'include' ) {
				include_once( $files );
			}
		}
	}

	/**
	 * Set Plugin Text Domain
	 */
	public function after_plugins_loaded() {
		load_plugin_textdomain( CHL_WC_TXT, false, CHL_WC_LANGUAGE_PATH );
	}

	/**
	 * load translated mo file based on wp settings
	 */
	public function load_plugin_mo_files( $mofile, $domain ) {
		if ( CHL_WC_TXT === $domain ) {
			return CHL_WC_LANGUAGE_PATH . '/' . get_locale() . '.mo';
		}

		return $mofile;
	}

	/**
	 * Define Required Constant
	 */
	private function define_constant() {
		$this->define( 'CHL_WC_NAME', 'SwissPostLabels' ); # Plugin Name
		$this->define( 'CHL_WC_SLUG', 'chlabels-wc' ); # Plugin Slug
		$this->define( 'CHL_WC_DB', 'CHL_wc_' ); # Plugin Slug
		$this->define( 'CHL_WC_PATH', plugin_dir_path( __FILE__ ) ); # Plugin DIR
		$this->define( 'CHL_WC_LANGUAGE_PATH', CHL_WC_PATH . 'languages' );
		$this->define( 'CHL_WC_TXT', 'woocommerce-swisspost-labels' ); #plugin lang Domain
		$this->define( 'CHL_WC_URL', plugins_url( '', __FILE__ ) );
		$this->define( 'CHL_WC_FILE', plugin_basename( __FILE__ ) );
		$this->define( 'CHL_WC_V', $this->version );
	}

	/**
	 * Define constant if not already set
	 *
	 * @param  string $name
	 * @param  string|bool $value
	 */
	protected function define( $key, $value ) {
		if ( ! defined( $key ) ) {
			define( $key, $value );
		}
	}

	/**
	 * What type of request is this?
	 * string $type ajax, frontend or admin
	 * @return bool
	 */
	private function is_request( $type ) {
		switch ( $type ) {
			case 'admin' :
				return is_admin();
			case 'ajax' :
				return defined( 'DOING_AJAX' );
			case 'cron' :
				return defined( 'DOING_CRON' );
			case 'frontend' :
				return ( ! is_admin() || defined( 'DOING_AJAX' ) ) && ! defined( 'DOING_CRON' );
		}
	}
}

new SwissPostLabels;
?>