FROM alpine:3.7

# basics
RUN apk add --no-cache \
  bash \
  curl \
  ca-certificates \
  less

# wait-for-it
ADD https://git.io/vyCoJ /usr/local/bin/wait-for-it
RUN chmod a+rx /usr/local/bin/wait-for-it

# apache and php
RUN apk add --no-cache \
  apache2 \
  php7 \
  php7-apache2 \
  php7-json \
  php7-iconv \
  php7-openssl \
  php7-mcrypt \
  php7-mbstring \
  php7-soap \
  php7-zip \
  php7-mysqli \
  php7-gd \
  php7-phar \
  php7-gettext \
  php7-curl \
  php7-ctype \
  php7-session \
  php7-exif \
  php7-opcache
RUN cp /usr/bin/php7 /usr/bin/php && \
  mkdir /run/apache2 && \
  chmod a+rw /run/apache2 && \
  adduser -S www-data www-data

# wp-cli
RUN curl -o /usr/local/bin/wp https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
RUN chmod a+x /usr/local/bin/wp



# wordpress
RUN mkdir -p /opt && \
  curl -sSL "https://wordpress.org/wordpress-4.9.7.tar.gz" | tar -xzf - -C /opt && \
  chown -R www-data:www-data /opt/wordpress
COPY docker/config/wp-config.php /opt/wordpress/wp-config.php
COPY docker/config/wp-cli.yml /wp-cli.yml

# config
COPY docker/config/apache2.conf /etc/apache2/httpd.conf
COPY docker/config/php.ini /etc/php7/conf.d/wordpress.ini

WORKDIR /opt/wordpress/wp-content/plugins/swiss-post-labels
USER www-data
ENTRYPOINT ["httpd", "-DFOREGROUND"]
CMD []
