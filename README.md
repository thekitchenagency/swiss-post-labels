swiss-post-labels
=================

WooCommerce plugin for printing [Swiss Post «Barcode»][post-barcode-webservice] shipping labels.

[post-barcode-webservice]: https://www.post.ch/en/business/a-z-of-subjects/dropping-off-mail-items/business-sending-letters/barcode-support

About
-----
TODO


Installation
------------

This plugin is not published to the Wordpress Plugin directory.
Instead you can install it via manual upload in WordPress, or using *Composer*.

See [`CHANGELOG.md`](/CHANGELOG.md) for version information.

### Manual Installation

1. Download the plugin as a ZIP file: [`swiss-post-labels-master.zip`][download-zip].
2. Upload the ZIP file inside the WordPress admin website, according to the [Manual Installation][wordpress-manual-installation] procedure.

[download-zip]: https://gitlab.com/thekitchenagency/swiss-post-labels/-/archive/master/swiss-post-labels-master.zip
[wordpress-manual-installation]: https://codex.wordpress.org/Managing_Plugins#Manual_Plugin_Installation

### Install with Composer

This plugin is published on Packagist as [`thekitchenagency/swiss-post-labels`][packagist-package].

The package uses [`composer/installers`][composer-installers] to allow installing to a custom folder like `wp-content/plugins/`.
Its package type is "wordpress-plugin".
This is the same type that [WPackagist][wpackagist] uses, so you can easily mix it with packages installed from that source.

[packagist-package]: https://packagist.org/packages/thekitchenagency/swiss-post-labels
[composer-installers]: https://github.com/composer/installers
[wpackagist]: https://wpackagist.org/

1: Create or adjust your project's `composer.json` like this:
```json
{
  "name": "...",
  "require": {
    "thekitchenagency/swiss-post-labels": "*"
  },
  "extra": {
    "installer-paths": {
      "wp-content/plugins/{$name}/": [
        "type:wordpress-plugin"
      ]
    }
  }
}
```

2: Install with:
```
composer install
```


Usage
-----
TODO


Development
-----------
See [`HACKING.md`](/HACKING.md).
